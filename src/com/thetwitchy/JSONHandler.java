package com.thetwitchy;

import static burp.api.montoya.http.handler.RequestToBeSentAction.continueWith;
import static burp.api.montoya.http.handler.ResponseReceivedAction.continueWith;

import burp.api.montoya.MontoyaApi;
import burp.api.montoya.core.Annotations;
import burp.api.montoya.core.HighlightColor;
import burp.api.montoya.http.handler.HttpHandler;
import burp.api.montoya.http.handler.HttpRequestToBeSent;
import burp.api.montoya.http.handler.HttpResponseReceived;
import burp.api.montoya.http.handler.RequestToBeSentAction;
import burp.api.montoya.http.handler.ResponseReceivedAction;
import burp.api.montoya.http.message.requests.MalformedRequestException;
import burp.api.montoya.http.message.responses.HttpResponse;
import java.io.ByteArrayInputStream;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

/**
 * This class is the main handler that will be used to process JSON responses.
 * It modifies JSON responses according to rules set by the user.
 */
public class JSONHandler implements HttpHandler {
    private MontoyaApi api;
    private AXBot9000Tab axbotTab;

    public JSONHandler(MontoyaApi api, AXBot9000Tab axbotTab) {
        this.api = api;
        this.axbotTab = axbotTab;
    }

    @Override
    public RequestToBeSentAction handleHttpRequestToBeSent(HttpRequestToBeSent requestToBeSent) {
        return continueWith(requestToBeSent);
    }

    @Override
    public ResponseReceivedAction handleHttpResponseReceived(
            HttpResponseReceived responseReceived) {
        if (axbotTab.isEnabled()) {
            // Obviously only work if the extension is enabled.

            // Check to make sure the request is in scope.
            if (!responseReceived.initiatingRequest().isInScope()) {
                // Not in scope, do nothing.
                return continueWith(responseReceived);
            }

            // Next, check to make sure the Content-Type is application/json
            String contentType = responseReceived.headerValue("Content-Type");
            if (contentType == null || !contentType.contains("application/json")) {
                // Not JSON, do nothing.
                return continueWith(responseReceived);
            }

            // Last, check to make sure the path matches the regex.
            String path;
            try {
                path = responseReceived.initiatingRequest().path();
            } catch (MalformedRequestException e) {
                // If we can't get the path, just return.
                return continueWith(responseReceived);
            }

            if (!path.matches(axbotTab.getPathRegex())) {
                // Path doesn't match, do nothing.
                return continueWith(responseReceived);
            }

            // If we reach here, then the request is in-scope, is JSON, and matches the path
            // regex for performing replacements.
            try {
                // Allocate a new JSON object that will hold the changes.
                // JsonObjectBuilder newJsonBuilder = Json.createObjectBuilder();

                // First, turn the response body into a JSON object.
                JsonParser parser = Json.createParser(
                        new ByteArrayInputStream(responseReceived.body().getBytes()));

                // From here, we have two paths. The first is that the response is a JSON object
                // directly, and we can just start processing it. The seond possibility is that
                // the response is a JSON array, and we need to iterate over the array and
                // process each object in the array. There is no good way to do this cleanly and
                // handle both possibilities.

                // First make sure there's a START_OBJECT or START_ARRAY event, and use that to
                // figure out what we need to do.
                if (!parser.hasNext()) {
                    // No events, so we can't do anything.
                    Annotations annotations = responseReceived.annotations();
                    annotations = annotations.withHighlightColor(HighlightColor.YELLOW);
                    annotations = annotations.withNotes(
                            "AXBot9000: No JSON events found in response body.");
                    return continueWith(responseReceived, annotations);
                }

                Event firstEvent = parser.next();
                String newResponseStr = "";

                if (firstEvent == Event.START_ARRAY) {
                    // The response is a JSON array.
                    newResponseStr = parseArray(parser).toString();
                } else if (firstEvent == Event.START_OBJECT) {
                    // The response is a JSON object, so go directly to JSON object processing.
                    newResponseStr = this.parseObject(parser).toString();
                } else {
                    // The response is neither a JSON array nor a JSON object, so we can't do
                    // anything.
                    Annotations annotations = responseReceived.annotations();
                    annotations = annotations.withHighlightColor(HighlightColor.YELLOW);
                    annotations = annotations
                            .withNotes("AXBot9000: Response body is neither a JSON "
                                    + "array nor a JSON object.");
                    return continueWith(responseReceived, annotations);
                }

                // Now that we have the new response, we need to replace the old response.
                HttpResponse newResponse = responseReceived.withBody(newResponseStr);
                return continueWith(newResponse);
            } catch (Exception e) {
                // Something went wrong with trying to parse the response JSON. Tag it and add a
                // comment.
                Annotations annotations = responseReceived.annotations();
                annotations = annotations.withHighlightColor(HighlightColor.YELLOW);
                annotations = annotations.withNotes("AXBot9000: JSON response parse error: "
                        + e.getMessage());
                return continueWith(responseReceived, annotations);
            }
        }
        return continueWith(responseReceived);
    }

    private JsonArray parseArray(JsonParser parser) {
        JsonArrayBuilder newJsonArrayBuilder = Json.createArrayBuilder();

        // Process each object in the array.
        boolean endProcessing = false;
        while (!endProcessing) {
            Event parserEvent = parser.next();

            switch (parserEvent) {
                case END_ARRAY:
                    endProcessing = true;
                    break;
                case VALUE_STRING:
                    if (axbotTab.replaceNext()) {
                        // Replace it with a payload
                        String randomPayload = axbotTab.getRandomPayload();
                        newJsonArrayBuilder.add(randomPayload);
                    } else {
                        // Don't.
                        newJsonArrayBuilder.add(parser.getString());
                    }
                    break;
                case VALUE_NUMBER:
                    if (axbotTab.replaceNonStringValues() && axbotTab.replaceNext()) {
                        // Replace it with a payload. Note that this changes the value type from a
                        // number to a string.
                        newJsonArrayBuilder.add(axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        newJsonArrayBuilder.add(parser.getBigDecimal());
                    }
                    break;
                case VALUE_TRUE:
                    if (axbotTab.replaceNonStringValues() && axbotTab.replaceNext()) {
                        // Replace it with a payload. Note that this changes the value type from a
                        // boolean to a string.
                        newJsonArrayBuilder.add(axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        newJsonArrayBuilder.add(true);
                    }
                    break;
                case VALUE_FALSE:
                    if (axbotTab.replaceNonStringValues() && axbotTab.replaceNext()) {
                        // Replace it with a payload. Note that this changes the value type from a
                        // boolean to a string.
                        newJsonArrayBuilder.add(axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        newJsonArrayBuilder.add(false);
                    }
                    break;
                case START_OBJECT:
                    newJsonArrayBuilder.add(this.parseObject(parser));
                    break;
                case START_ARRAY:
                    newJsonArrayBuilder.add(this.parseArray(parser));
                    break;
                default:
                    api.logging().logToError(
                            "Error: A parsed event within an array returned an "
                                    + "unexpected event type. This should not"
                                    + " happen, and is likely a bug. EVENT_TYPE = "
                                    + parserEvent.toString());
                    break;
            }
        }
        return newJsonArrayBuilder.build();
    }

    private JsonObject parseObject(JsonParser parser) {
        String currentKey = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        boolean endProcessing = false;

        while (!endProcessing) {
            Event parserEvent = parser.next();

            switch (parserEvent) {
                case KEY_NAME:
                    currentKey = parser.getString();
                    break;
                case VALUE_STRING:
                    if (axbotTab.replaceNext()) {
                        // Replace it with a payload
                        builder.add(currentKey, axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        builder.add(currentKey, parser.getString());
                    }
                    break;
                case VALUE_NUMBER:
                    if (axbotTab.replaceNonStringValues() && axbotTab.replaceNext()) {
                        // Replace it with a payload. Note that this changes the value type from a
                        // number to a string.
                        builder.add(currentKey, axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        builder.add(currentKey, parser.getBigDecimal());
                    }
                    break;
                case VALUE_TRUE:
                    if (axbotTab.replaceNonStringValues() && axbotTab.replaceNext()) {
                        // Replace it with a payload. Note that this changes the value type from a
                        // boolean to a string.
                        builder.add(currentKey, axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        builder.add(currentKey, true);
                    }
                    break;
                case VALUE_FALSE:
                    if (axbotTab.replaceNonStringValues() && axbotTab.replaceNext()) {
                        // Replace it with a payload. Note that this changes the value type from a
                        // boolean to a string.
                        builder.add(currentKey, axbotTab.getRandomPayload());
                    } else {
                        // Don't.
                        builder.add(currentKey, false);
                    }
                    break;
                case VALUE_NULL:
                    builder.addNull(currentKey);
                    break;
                case START_OBJECT:
                    builder.add(currentKey, this.parseObject(parser));
                    break;
                case END_OBJECT:
                    endProcessing = true;
                    break;
                case START_ARRAY:
                    builder.add(currentKey, this.parseArray(parser));
                    break;
                case END_ARRAY:
                    // This should never happen, since we're only calling this method when we know
                    // that this is an object.
                    break;
                default:
                    api.logging().logToError("Unknown JSON Event type: " + parserEvent.toString());
                    break;
            }
        }
        return builder.build();
    }

}
