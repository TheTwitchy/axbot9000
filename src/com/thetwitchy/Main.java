package com.thetwitchy;

import burp.api.montoya.BurpExtension;
import burp.api.montoya.MontoyaApi;

/**
 * Main entry point for the Burp extension. This class is instantiated by Burp.
 */
public class Main implements BurpExtension {
    private AXBot9000Tab axbotTab;

    @Override
    public void initialize(MontoyaApi api) {

        String extVersion = getClass().getPackage().getImplementationVersion();

        if (extVersion == null) {
            // For development, the version is not set in the manifest, so we'll just
            // use this. This should work for actual JAR builds. See
            // https://stackoverflow.com/a/33020743
            extVersion = "DEVELOPMENT";
        }

        api.extension().setName("AXBot9000");

        api.logging().logToOutput("INFO: Starting AXBot9000 Burp Extension...");

        api.logging().logToOutput("INFO: Reported Burp ext version: " + extVersion);

        // Regsiter our tab with Burp.
        axbotTab = new AXBot9000Tab(api);
        api.userInterface().registerSuiteTab("AXBot9000", axbotTab);

        // Register our HTTP handler with Burp.
        api.http().registerHttpHandler(new JSONHandler(api, axbotTab));

        api.logging().logToOutput("INFO: AXBot9000 Burp Extension loaded successfully!");
    }
}