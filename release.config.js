module.exports = {
    "branches": ['main'],
    "plugins": [
        "@semantic-release/commit-analyzer",
        "gradle-semantic-release-plugin",
        [
            "@semantic-release/git",
            {
                "assets": [
                    "gradle.properties"
                ]
            }
        ],
        "@semantic-release/release-notes-generator",
        [
            "@semantic-release/gitlab",
            {
                "assets": [
                    { "path": "build/libs/axbot9000.jar", "label": "AXBot9000 Extension" }
                ]
            }
        ],
    ]
}