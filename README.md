# AXBot9000

API XSS Bot version 9000-ish

![axbot logo](/docs/axbot_1.png)


## FAQ
* **What is this?** - A Burp extension to automate finding cross-site-scripting in single-page-app-style web applications using JSON APIs. 

* **What is this NOT?** - This is not a comprehensive XSS scanner, it is rather a lightweight script that can be used while regularly browsing an app like normal to find spots where XSS may be possible via user input. 

* **How is this different from other XSS and web app scanners?** - Because modern web apps often do all the view rendering client-side, and because view-rendering is where XSS bugs happen, the old paradigm of inserting XSS payloads in *requests* to see if they're unsafely rendered is no longer efficient. Rather, we should be targeting the *responses* to determine if fields are vulnerable. This script enables this new workflow, and automates it to some degree.

* **How does it work?** - AXBot9000 works by randomly replacing the values in any responses where `Content-Type` is `application/json` with user-obvious cross-site scripting payloads that will inform the bug hunter that there is a vulnerable field that renders JavaScript. Although frameworks like React and AngularJS have standard ways to bypass the input protections that prevent XSS attacks, they're often buried under other layers of frameworks, and may use non-standard notation. This script will work on *any* JS framework that dynamically renders pages from JSON APIs (probably). It's a good smoke test, and generally requires very little heavy lifting from the bug hunter.

* **How can I try it out?** - If you'd like to test AXBot9000, try it out against [this vulnerable web app](https://gitlab.com/TheTwitchy/vulnd-json-xss).

* **Why isn't this just a Burp extension?** - It is now, smart guy.

## High Level Workflow
1. Install and run the [AXBot9000 Burp Extension](https://gitlab.com/TheTwitchy/axbot9000/-/releases) into Burp while performing a penetration test. Make sure the scope is set correctly, as AXBot9000 will only process in-scope requests.
2. Browse around and use the app as normal, adjusting and optimizing the AXBot9000 configuration to minimize view and rendering errors as a result of malicious payload insertions.
3. Wait for an alert box, or other indicator that a payload is being consumed unsafely, to pop up, noting the unique identifer that it presents. This indicator will depend on the payload, but it could be strangly bolded text, rendering errors as a result of inserting HTML, or the like.
4. Search through Burp history for the unique identifier attached to the indicator, and determine if the response is the result of a request that can be changed or influenced via user input.
5. If so, you have a cross-site scripting finding (usually stored, not reflected).
6. Refresh your pages every so often to cause a new set of values to be replaced (even in repeated calls). The random replacement means that it could target a new field that hasn't yet been processed.
7. Repeat entire workflow until you can swim in your bug bounty money.

## Optimization and Configuration
AXBot9000 requires some fine-tuning on a per-application basis. Some apps may work directly out of the box, and others applications may be finicky enough to require near constant tuning. The follow details some common "knobs" and "switches" that you can mess with in AXBot9000.

### Replace Chance
* Valid range: 0-100
* This is the main "knob" you'll be using during testing. This defines, for every value in a JSON call, how likely it is that the value will be replaced with an AXBot9000 payload. That means that given the follow response and `REPLACE_CHANCE=25`:
> `{"a":"val1", "b":"val2", "c":"val3", "d":"val4"}`

on average, one of the four values will be replaced with a new value. When `REPLACE_CHANCE=0`, no replacement will take place, and when `REPLACE_CHANCE=100`, every single valid value in the response will be replaced.
* Naturally this begs the question of when we shouldn't just set `REPLACE_CHANCE=100` immediately and all the time. The answer is that the payloads tend to break the app, especially the view rendering so that the app appears to have loaded incorrectly. In the best case scenario, you'll see the exact same app as before, just with some values replaced with odd markup, but in practice higher `REPLACE_CHANCE` values tend to break apps more frequently.
* It's recommended that you start with a `REPLACE_CHANCE` of 20%-40% for new apps, and turn it up if the app tends to be usable, and turn it down if the app breaks often. You should strive to have as high a `REPLACE_CHANCE` as possible while still having a usable app.

### Replace Non-string JSON Values
* This defines whether or not you want to include non-string values in your payload processing. For example, when `PROCESS_NON_STR_VALUES=True`, the response
> `{"a":"val1", "b":false, "c":1.2345}`

can become

> `{"a":"AXBOT9K_PAYLOAD", "b":"AXBOT9K_PAYLOAD", "c":"AXBOT9K_PAYLOAD"}`

* Notice that the types of `b` and `c` change to a string. 
* This can result in some unintended behavior since the app might suddenly be trying to do integer or boolean operations on a string.

### Path Regular Expression
* This regular expression defines the scope of requests that will be processed. Anything falling outside the scope will not be processed by AxBot9000.
* Some example scopes:
    * All requests valid - `/.*`
    * All requests under `/api/` - `/api/.*`
